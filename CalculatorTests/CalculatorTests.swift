//
//  CalculatorTests.swift
//  CalculatorTests
//
//  Created by Henning Vestergaard Poulsen on 12/08/2016.
//  Copyright © 2016 Gninneh. All rights reserved.
//

import XCTest
@testable import Calculator

class CalculatorTests: XCTestCase {
	var vc: ViewController!
	var calc: Calc!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
		let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
		vc = storyboard.instantiateInitialViewController() as! ViewController
		calc = Calc()

    }
	
	func testCalculateExpression(){
		XCTAssertFalse(calc.calculateExpression("2+2") == 5, "Just testing the test")
		XCTAssert(calc.calculateExpression("2+6") == 8, "+")
		XCTAssert(calc.calculateExpression("2+6x2") == 14, "+ *")
		XCTAssert(calc.calculateExpression("6x2+4") == 16, "* +")
		XCTAssert(calc.calculateExpression("2+2+2+2") == 8, "+ + + ")
		XCTAssert(calc.calculateExpression("2+2+2+2-1") == 7, "+ + + - ")
		XCTAssert(calc.calculateExpression("1-1") == 0, "-")
		XCTAssert(calc.calculateExpression("0-1") == -1, "- negative result")
		XCTAssert(calc.calculateExpression("-1") == -1, "unary minus")
		XCTAssert(calc.calculateExpression("-1x2") == -2, "negative 1*2")
		XCTAssert(calc.calculateExpression("-10/2") == -5, "- /")
		XCTAssert(calc.calculateExpression("2-3+4x5/10") == 1, "- + * /")
		XCTAssert(calc.calculateExpression("100/10x3-5+2") == 27, "/ * - +")
		XCTAssert(calc.calculateExpression("2^3/2+3") == 7, "^ / +")

	}
	
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
