import UIKit

enum Operator {
	case PLUS;
	case MINUS;
	case MULTIPLY;
	case DIVIDE;
}

enum Termtype {
	case TERM
	case NUMBER
}

func parseTerm(mathString: String) -> Double {
	
	let operatorsInReverseOrderOfPrecedence = ["+", "-", "%", "*", "/", "^"]
	
	for operatorChar in operatorsInReverseOrderOfPrecedence {
		if(mathString.rangeOfString(operatorChar) != nil){
			let operands = mathString.componentsSeparatedByCharactersInSet(NSCharacterSet.init(charactersInString: operatorChar))
			for var operand in operands {
				if(operand.isEmpty){
					operand = "0" // in the case where unary minus is used "-2+3" => replace string with "0-2+3"
				}
			}

			var result = 0.0
			switch operatorChar {
			case "+":
				for operand in operands {
					result = result + Double(parseTerm(operand))
				}
				break
			case "-":
				result = Double(parseTerm(operands[0]))
				for i in 1...(operands.count-1) {
					result = result - Double(parseTerm(operands[i]))
				}
				break
			case "*":
				result = 1.0
				for operand in operands {
					result = result * Double(parseTerm(operand))
				}
				break
			case "/":
				result = Double(parseTerm(operands[0]))
				for i in 1...(operands.count-1) {
					result = result / Double(parseTerm(operands[i]))
				}
				break
			case "^":
				result = Double(parseTerm(operands[0]))
				for i in 1...(operands.count-1) {
					result = pow(result,Double(parseTerm(operands[i])))
				}
				break
			case "%":
				result = Double(parseTerm(operands[0]))
				for i in 1...(operands.count-1) {
					result = result % Double(parseTerm(operands[i]))
				}
				break
			default:
				result = 0.0
				break
			}
			return result
		}
	}
	return Double(mathString)!
}
parseTerm("10-1-1-1-1+1-1*10+1+1+1*10")
parseTerm("0-1-1-1-1+2*2-2")
parseTerm("10/10*5+4/2/2")
parseTerm("10*2^3/10")
parseTerm("10%22+4")











