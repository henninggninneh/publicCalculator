//
//  ViewController.swift
//  Calculator
//
//  Created by Henning Vestergaard Poulsen on 12/08/2016.
//  Copyright © 2016 Gninneh. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

	@IBOutlet var Calcview: UITextView!
	@IBOutlet var Input: UITextField!
	
	
	
	// BUTTONS
	@IBOutlet var BUTTON_1: UIButton!
	@IBOutlet var BUTTON_2: UIButton!
	@IBOutlet var BUTTON_3: UIButton!
	@IBOutlet var BUTTON_4: UIButton!
	@IBOutlet var BUTTON_5: UIButton!
	@IBOutlet var BUTTON_6: UIButton!
	@IBOutlet var BUTTON_7: UIButton!
	@IBOutlet var BUTTON_8: UIButton!
	@IBOutlet var BUTTON_9: UIButton!
	@IBOutlet var BUTTON_0: UIButton!
	@IBOutlet var BUTTON_C: UIButton!
	@IBOutlet var BUTTON_POINT: UIButton!
	@IBOutlet var BUTTON_DEL: UIButton!
	@IBOutlet var BUTTON_HAT: UIButton!
	@IBOutlet var BUTTON_FORMAT: UIButton!
	@IBOutlet var BUTTON_PLUS: UIButton!
	@IBOutlet var BUTTON_MINUS: UIButton!
	@IBOutlet var BUTTON_MULTIPLY: UIButton!
	@IBOutlet var BUTTON_DIVIDE: UIButton!
	@IBOutlet var BUTTON_EQUALS: UIButton!
	
	enum NumberFormat {
		case NUM;
		case TEXT;
	}
	var settingNumberFormat: NumberFormat = .NUM
	var calc = Calc()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.

		BUTTON_1.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_2.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_3.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_4.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_5.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_6.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_7.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_8.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_9.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_0.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_C.addTarget(			self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_POINT.addTarget(		self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_DEL.addTarget(		self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_HAT.addTarget(		self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_FORMAT.addTarget(	self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_PLUS.addTarget(		self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_MINUS.addTarget(		self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_MULTIPLY.addTarget(	self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_DIVIDE.addTarget(	self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		BUTTON_EQUALS.addTarget(	self, action: #selector(ViewController.buttonClicked(_:)), forControlEvents: .TouchUpInside)
		

//		Following two lines obsolete after a custom keyboard extension was decided unsuited for this application
//		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
//		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
	}

	
	func buttonClicked(sender: AnyObject?) {
//		print("BUTTON CLICKED METHOD CALLED " + (sender as! UIButton).currentTitle!)

		switch (sender as! UIButton).currentTitle! {
		case "0":
			Input.text = Input.text! + "0"
			break
		case "1":
			Input.text = Input.text! + "1"
			break
		case "2":
			Input.text = Input.text! + "2"
			break
		case "3":
			Input.text = Input.text! + "3"
			break
		case "4":
			Input.text = Input.text! + "4"
			break
		case "5":
			Input.text = Input.text! + "5"
			break
		case "6":
			Input.text = Input.text! + "6"
			break
		case "7":
			Input.text = Input.text! + "7"
			break
		case "8":
			Input.text = Input.text! + "8"
			break
		case "9":
			Input.text = Input.text! + "9"
			break
		case "C":
			Input.text = ""
			break
		case "←":
//			Input.deleteBackward() // Since fields have been disabled, there is no insertion field where this works
			if(!(Input.text?.isEmpty)!){
				Input.text = Input.text?.substringToIndex(Input.text!.endIndex.predecessor())
			}
			break
		case "÷":
			Input.text = Input.text! + "/"
			break
		case "✕":
			Input.text = Input.text! + "x"
			break
		case "-":
			Input.text = Input.text! + "-"
			break
		case "+":
			Input.text = Input.text! + "+"
			break
		case "^":
			Input.text = Input.text! + "^"
			break
		case "=":
			if(!(Input.text?.isEmpty)!){
				let result = calc.calculateExpression(Input.text!)
				if(!result.isNaN){
					switch settingNumberFormat {
					case .NUM:
						Calcview.text = Calcview.text + ">" + Input.text! + "\n" + NSNumberFormatter.localizedStringFromNumber(result, numberStyle: NSNumberFormatterStyle.DecimalStyle) + "\n"
						break
					case .TEXT:
						// just for fun
						Calcview.text = Calcview.text + ">" + Input.text! + "\n" + NSNumberFormatter.localizedStringFromNumber(result, numberStyle: NSNumberFormatterStyle.SpellOutStyle) + "\n"
						break
					}
					Input.text = ""
				} else {
					let errorMessage = NSLocalizedString("syntaxError", comment: "There is an error in your syntax")
					Calcview.text = Calcview.text + ">" + Input.text! + "\n" + errorMessage + "\n"
				}
				// scroll to last line
				if(Calcview.contentSize.height > Calcview.bounds.size.height){
					let bottomOffset = CGPointMake(0, Calcview.contentSize.height - Calcview.bounds.size.height)
					Calcview.setContentOffset(bottomOffset, animated: true)
				}
			}
			break
		case ".":
			Input.text = Input.text! + "."
			break
		case "num":
				settingNumberFormat = .TEXT
				BUTTON_FORMAT.setTitle("text", forState: UIControlState.Normal)
			break
		case "text":
				settingNumberFormat = .NUM
				BUTTON_FORMAT.setTitle("num", forState: UIControlState.Normal)
			break
		default:
			break
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// USED BY CUSTOM KEYBOARD EXTENSION - WHICH APPEARS TO BE NOT SUITABLE FOR A CALCULATOR APP, BECAUSE THE USER NEEDS TO GO INTO SETTINGS TO MANUALLY INSTALL THE KEYBOARD. INSTEAD USING UIBUTTONS
	func keyboardWillShow(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
			if view.frame.origin.y == 0{
				self.view.frame.origin.y -= keyboardSize.height
			}
			else {
				
			}
		}
		
	}
	
	func keyboardWillHide(notification: NSNotification) {
		if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
			if view.frame.origin.y != 0 {
				self.view.frame.origin.y += keyboardSize.height
			}
			else {
				
			}
		}
	}

}

