//
//  Calc.swift
//  Calculator
//
//  Created by Henning Vestergaard Poulsen on 14/08/2016.
//  Copyright © 2016 Gninneh. All rights reserved.
//

import Foundation

class Calc {
	init(){
		
	}
	func calculateExpression(expression: String) -> Double {
		
		let operatorsInReverseOrderOfPrecedence = ["+", "-", "%", "x", "/", "^"]
		
		for operatorChar in operatorsInReverseOrderOfPrecedence {
			if(expression.rangeOfString(operatorChar) != nil){
				let operands = expression.componentsSeparatedByCharactersInSet(NSCharacterSet.init(charactersInString: operatorChar))
				for var operand in operands {
					if(operand.isEmpty){
						operand = "0" // in the case where unary minus is used "-2+3" => replace string with "0-2+3"
					}
				}
				
				var result = 0.0
				switch operatorChar {
				case "+":
					for operand in operands {
						if(operand.isEmpty){
							return Double.NaN
						}
						result = result + Double(calculateExpression(operand))
					}
					break
				case "-":
					if(operands[0].isEmpty){
						result = 0
					} else {
						result = Double(calculateExpression(operands[0]))
					}
					for i in 1...(operands.count-1) {
						if(operands[i].isEmpty){
							return Double.NaN
						}
						result = result - Double(calculateExpression(operands[i]))
					}
					break
				case "x":
					result = 1.0
					for operand in operands {
						if(operand.isEmpty){
							return Double.NaN
						}
						result = result * Double(calculateExpression(operand))
					}
					break
				case "/":
					if(operands[0].isEmpty){
						return Double.NaN
					} else {
						result = Double(calculateExpression(operands[0]))
					}
					for i in 1...(operands.count-1) {
						if(operands[i].isEmpty){
							return Double.NaN
						}
						result = result / Double(calculateExpression(operands[i]))
					}
					break
				case "^":
					if(operands[0].isEmpty){
						return Double.NaN
					} else {
						result = Double(calculateExpression(operands[0]))
					}
					for i in 1...(operands.count-1) {
						if(operands[i].isEmpty){
							return Double.NaN
						}
						result = pow(result,Double(calculateExpression(operands[i])))
					}
					break
				case "%":
					if(operands[0].isEmpty){
						return Double.NaN
					} else {
						result = Double(calculateExpression(operands[0]))
					}
					for i in 1...(operands.count-1) {
						if(operands[i].isEmpty){
							return Double.NaN
						}
						result = result % Double(calculateExpression(operands[i]))
					}
					break
				default:
					result = 0.0
					break
				}
				return result
			}
		}
		return Double(expression)!
	}
}