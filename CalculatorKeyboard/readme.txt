

It seems a custom keyboard can not be used without the user installing it manually through Settings > General > Keyboard > Keyboards > Add new Keyboard
So it is not suitable for this where there should just be a numpad ready from start.