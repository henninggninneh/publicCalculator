//
//  KeyboardViewController.swift
//  CalculatorKeyboard
//
//  Created by Henning Vestergaard Poulsen on 12/08/2016.
//  Copyright © 2016 Gninneh. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

//    @IBOutlet var nextKeyboardButton: UIButton!

	
	
	func createButtonWithTitle(title: String) -> UIButton {
		
		let button = UIButton.init(type: .System) as UIButton
		button.frame = CGRectMake(0, 0, 20, 20)
		button.setTitle(title, forState: .Normal)
		button.sizeToFit()
		button.titleLabel?.font = UIFont.systemFontOfSize(15)
		button.translatesAutoresizingMaskIntoConstraints = false
		button.backgroundColor = UIColor(white: 1.0, alpha: 1.0)
		button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
		
		button.addTarget(self, action: #selector(KeyboardViewController.didTapButton(_:)), forControlEvents: .TouchUpInside)
		
		return button
	}
	
	func createRowOfButtons(buttonTitles: [String]) -> UIView {
		
		var buttons = [UIButton]()
		let keyboardRowView = UIView(frame: CGRectMake(0, 0, 320, 50))
		
		for buttonTitle in buttonTitles{
			
			let button = createButtonWithTitle(buttonTitle)
			buttons.append(button)
			keyboardRowView.addSubview(button)
		}
		
		addIndividualButtonConstraints(buttons, mainView: keyboardRowView)
		
		return keyboardRowView
	}


	func didTapButton(sender: AnyObject?) {
		
		let button = sender as! UIButton
		let title = button.titleForState(.Normal)! as String
		let proxy = textDocumentProxy as UITextDocumentProxy
		
		switch title {
		case "÷" :
			proxy.insertText("/")
		case "C" :
			proxy.deleteBackward()
		case "RETURN" :
			proxy.insertText("\n")
		case "SPACE" :
			proxy.insertText(" ")
		case "CHG" :
			self.advanceToNextInputMode()
		default :
			proxy.insertText(title)
		}
		
		UIView.animateWithDuration(0.1, animations: {
			button.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2)
			}, completion: {(_) -> Void in
				button.transform =
				CGAffineTransformScale(CGAffineTransformIdentity, 1, 1)
		})
	}
	
	
	
	
	
	
	
	
	
    override func updateViewConstraints() {
        super.updateViewConstraints()
    
        // Add custom view sizing constraints here
    }

    override func viewDidLoad() {
        super.viewDidLoad()
		
		let buttonTitles1 = ["C", "^", "%", "÷"]
		let buttonTitles2 = ["7", "8", "9", "✕"]
		let buttonTitles3 = ["4", "5", "6", "-"]
		let buttonTitles4 = ["1", "2", "3", "+"]
		let buttonTitles5 = ["CHG", "0", ".", "="]
		
		let row1 = createRowOfButtons(buttonTitles1)
		let row2 = createRowOfButtons(buttonTitles2)
		let row3 = createRowOfButtons(buttonTitles3)
		let row4 = createRowOfButtons(buttonTitles4)
		let row5 = createRowOfButtons(buttonTitles5)
		
		self.view.addSubview(row1)
		self.view.addSubview(row2)
		self.view.addSubview(row3)
		self.view.addSubview(row4)
		self.view.addSubview(row5)
		
		row1.translatesAutoresizingMaskIntoConstraints = false
		row2.translatesAutoresizingMaskIntoConstraints = false
		row3.translatesAutoresizingMaskIntoConstraints = false
		row4.translatesAutoresizingMaskIntoConstraints = false
		row5.translatesAutoresizingMaskIntoConstraints = false
		
		addConstraintsToInputView(self.view, rowViews: [row1, row2, row3, row4, row5])
		
		
		
		
//        self.nextKeyboardButton = UIButton(type: .System)
//		
//        self.nextKeyboardButton.setTitle(NSLocalizedString("Next Keyboard", comment: "Title for 'Next Keyboard' button"), forState: .Normal)
//        self.nextKeyboardButton.sizeToFit()
//        self.nextKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
//    
//        self.nextKeyboardButton.addTarget(self, action: #selector(advanceToNextInputMode), forControlEvents: .TouchUpInside)
//        
//        self.view.addSubview(self.nextKeyboardButton)
		
//        self.nextKeyboardButton.leftAnchor.constraintEqualToAnchor(self.view.leftAnchor).active = true
//        self.nextKeyboardButton.bottomAnchor.constraintEqualToAnchor(self.view.bottomAnchor).active = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.Dark {
            textColor = UIColor.whiteColor()
        } else {
            textColor = UIColor.blackColor()
        }
//        self.nextKeyboardButton.setTitleColor(textColor, forState: .Normal)
    }

	func addIndividualButtonConstraints(buttons: [UIButton], mainView: UIView){
		
		for (index, button) in buttons.enumerate() {
			
			let topConstraint = NSLayoutConstraint(item: button, attribute: .Top, relatedBy: .Equal, toItem: mainView, attribute: .Top, multiplier: 1.0, constant: 1)
			
			let bottomConstraint = NSLayoutConstraint(item: button, attribute: .Bottom, relatedBy: .Equal, toItem: mainView, attribute: .Bottom, multiplier: 1.0, constant: -1)
			
			var rightConstraint : NSLayoutConstraint!
			
			if index == buttons.count - 1 {
				
				rightConstraint = NSLayoutConstraint(item: button, attribute: .Right, relatedBy: .Equal, toItem: mainView, attribute: .Right, multiplier: 1.0, constant: -1)
				
			}else{
				
				let nextButton = buttons[index+1]
				rightConstraint = NSLayoutConstraint(item: button, attribute: .Right, relatedBy: .Equal, toItem: nextButton, attribute: .Left, multiplier: 1.0, constant: -1)
			}
			
			
			var leftConstraint : NSLayoutConstraint!
			
			if index == 0 {
				
				leftConstraint = NSLayoutConstraint(item: button, attribute: .Left, relatedBy: .Equal, toItem: mainView, attribute: .Left, multiplier: 1.0, constant: 1)
				
			}else{
				
				let prevtButton = buttons[index-1]
				leftConstraint = NSLayoutConstraint(item: button, attribute: .Left, relatedBy: .Equal, toItem: prevtButton, attribute: .Right, multiplier: 1.0, constant: 1)
				
				let firstButton = buttons[0]
				let widthConstraint = NSLayoutConstraint(item: firstButton, attribute: .Width, relatedBy: .Equal, toItem: button, attribute: .Width, multiplier: 1.0, constant: 0)
				
				mainView.addConstraint(widthConstraint)
			}
			
			mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
		}
	}
	func addConstraintsToInputView(inputView: UIView, rowViews: [UIView]){
		
		for (index, rowView) in rowViews.enumerate() {
			let rightSideConstraint = NSLayoutConstraint(item: rowView, attribute: .Right, relatedBy: .Equal, toItem: inputView, attribute: .Right, multiplier: 1.0, constant: -1)
			
			let leftConstraint = NSLayoutConstraint(item: rowView, attribute: .Left, relatedBy: .Equal, toItem: inputView, attribute: .Left, multiplier: 1.0, constant: 1)
			
			inputView.addConstraints([leftConstraint, rightSideConstraint])
			
			var topConstraint: NSLayoutConstraint
			
			if index == 0 {
				topConstraint = NSLayoutConstraint(item: rowView, attribute: .Top, relatedBy: .Equal, toItem: inputView, attribute: .Top, multiplier: 1.0, constant: 0)
				
			}else{
				
				let prevRow = rowViews[index-1]
				topConstraint = NSLayoutConstraint(item: rowView, attribute: .Top, relatedBy: .Equal, toItem: prevRow, attribute: .Bottom, multiplier: 1.0, constant: 0)
				
				let firstRow = rowViews[0]
				let heightConstraint = NSLayoutConstraint(item: firstRow, attribute: .Height, relatedBy: .Equal, toItem: rowView, attribute: .Height, multiplier: 1.0, constant: 0)
				
				inputView.addConstraint(heightConstraint)
			}
			inputView.addConstraint(topConstraint)
			
			var bottomConstraint: NSLayoutConstraint
			
			if index == rowViews.count - 1 {
				bottomConstraint = NSLayoutConstraint(item: rowView, attribute: .Bottom, relatedBy: .Equal, toItem: inputView, attribute: .Bottom, multiplier: 1.0, constant: 0)
				
			}else{
				
				let nextRow = rowViews[index+1]
				bottomConstraint = NSLayoutConstraint(item: rowView, attribute: .Bottom, relatedBy: .Equal, toItem: nextRow, attribute: .Top, multiplier: 1.0, constant: 0)
			}
			
			inputView.addConstraint(bottomConstraint)
		}
		
	}
	
}



